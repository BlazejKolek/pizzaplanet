﻿using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal.Data.Models;
using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Helpers
{
   public class AutoMapperModel
    {
        public static void CreateMap()
        {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<Ingredient, IngredientBl>();
                cfg.CreateMap<IngredientBl, Ingredient>();
                cfg.CreateMap<Sauce, SauceBl>();
                cfg.CreateMap<SauceBl, Sauce>();
                cfg.CreateMap<Pizza, PizzaBl>();
                cfg.CreateMap<PizzaBl, Pizza>();
                cfg.CreateMap<Order, OrderBl>();
                cfg.CreateMap<OrderBl, Order>();
                cfg.CreateMap<PizzaIngredient, PizzaIngredientBl>();
                cfg.CreateMap<PizzaIngredientBl, PizzaIngredient>();
                cfg.CreateMap<OrderPizza, OrderPizzaBl>();
                cfg.CreateMap<OrderPizzaBl, OrderPizza>();
                cfg.CreateMap<OrderSauce, OrderSauceBl>();
                cfg.CreateMap<OrderSauceBl, OrderSauce>();
                cfg.CreateMap<Pizza.Size, PizzaBl.Size>();
                cfg.CreateMap<PizzaBl.Size, Pizza.Size>();
                cfg.CreateMap<Client, ClientBl>();
                cfg.CreateMap<ClientBl, Client>();

            });
        }
    }
}
