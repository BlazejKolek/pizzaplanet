﻿using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Helpers
{
   internal class ModelsMapper
    {
        public IngredientBl MapIndgredientFromDataToBusinessModel(Ingredient ingredient)
        {
            return new IngredientBl
            {
               Id = ingredient.Id,
               Name = ingredient.Name
            };
        }

        public Ingredient MapIndgredientFromBusinessToDataModel(IngredientBl ingredientBl)
        {
            if (ingredientBl == null)
            {
                return null;
            }
            return new Ingredient
            {
                Id = ingredientBl.Id,
                Name = ingredientBl.Name
            };
        }

        public SauceBl MapSauceFromDataToBusinnes(Sauce sauce)
        {
            return new SauceBl
            {
                Id = sauce.Id,
                Name = sauce.Name,
                Price = sauce.Price
            };
        }

        public Sauce MapSauceFromBusinnesToData(SauceBl sauceBl)
        {
            if (sauceBl == null)
            {
                return null;
            }

            return new Sauce
            {
                Id = sauceBl.Id,
                Name = sauceBl.Name,
                Price = sauceBl.Price
            };
        }

        public PizzaBl MapPizzaFromDataToBusiness(Pizza pizza)
        {
            return new PizzaBl
            {
                Id = pizza.Id,
                Name = pizza.Name,
                Price = pizza.Price,
                PizzaSize = MappPizzaSizeFromDataToBusiness(pizza.PizzaSize),
                Sauce = MapSauceFromDataToBusinnes(pizza.Sauce),
                Ingredients = pizza.Ingredients?.Select(indgredient => MapIndgredientFromDataToBusinessModel(indgredient)).ToList()      
            };
        }

        public Pizza MapPizzaFromBusinessToData(PizzaBl pizzaBl)
        {
            if (pizzaBl == null)
            {
                return null;
            }

            return new Pizza
            {
                Id = pizzaBl.Id,
                Name = pizzaBl.Name,
                Price = pizzaBl.Price,
                PizzaSize = MappPizzaSizeFromBusinessToData(pizzaBl.PizzaSize),
                Sauce = MapSauceFromBusinnesToData(pizzaBl.Sauce),
                Ingredients = pizzaBl.Ingredients?.Select(indgredientBl => MapIndgredientFromBusinessToDataModel(indgredientBl)).ToList()
            };
        }

        public PizzaBl.Size MappPizzaSizeFromDataToBusiness(Pizza.Size pizzaSize)
        {
            switch (pizzaSize)
            {
                case Pizza.Size.S:
                    return PizzaBl.Size.S;
                case Pizza.Size.M:
                    return PizzaBl.Size.M;
                case Pizza.Size.L:
                    return PizzaBl.Size.L;
                default:
                    throw new Exception($"Cannot map {pizzaSize} to PizzaBl.Size type");
            }
        }

        public Pizza.Size MappPizzaSizeFromBusinessToData(PizzaBl.Size pizzaSize)
        {
            switch (pizzaSize)
            {
                case PizzaBl.Size.S:
                    return Pizza.Size.S;
                case PizzaBl.Size.M:
                    return Pizza.Size.M;
                case PizzaBl.Size.L:
                    return Pizza.Size.L;
                default:
                    throw new Exception($"Cannot map {pizzaSize} to Pizza.Size type");
            }
        }



        public OrderBl MapOrderFromDataToBusiness(Order order)
        {
            return new OrderBl
            {
                OrderId = order.OrderId,
                Price = order.Price,
                Pizzas = order.Pizzas?.Select(pizza => MapPizzaFromDataToBusiness(pizza)).ToList(),
                Sauces = order.Sauces?.Select(sauce => MapSauceFromDataToBusinnes(sauce)).ToList()
            };
        }

        public Order MapOrderFromBusinessToData(OrderBl orderBl)
        {
            var pizzas = orderBl.Pizzas?.Select(pizzaBl => MapPizzaFromBusinessToData(pizzaBl)).ToList();
            var sauces = orderBl.Sauces?.Select(sauceBl => MapSauceFromBusinnesToData(sauceBl)).ToList();

            return new Order
            {
                OrderId = orderBl.OrderId,
                Price = orderBl.Price,
                Pizzas = pizzas,
                Sauces = sauces
            };
        }
    }
}
