﻿using PizzaPlanet.Business.Logic.Models;

namespace PizzaPlanet.Business.Logic.Interfaces
{
    public interface IIngredientService
    {
        void AddIngredient(IngredientBl ingredient);
        void GetAllIngredients();
        IngredientBl ReturnIngredientById(int ingredient);
    }
}