﻿using PizzaPlanet.Business.Logic.Models;

namespace PizzaPlanet.Business.Logic.Interfaces
{
    public interface IOrderService
    {
        void AddOrder(OrderBl order);
        OrderPizzaBl AddPizzaToOrder(OrderBl newOrder, int pizzaToOrder, int howManyPizzasYouWant);
        OrderSauceBl AddSauceToOrder(int sauceToList, int howManySauce, bool isFree);
        bool CheckHaveAnyPizza();
        double ReturnOrderPrice(OrderBl newOrder);
    }
}