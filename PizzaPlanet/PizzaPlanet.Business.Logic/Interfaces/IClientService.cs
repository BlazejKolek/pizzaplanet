﻿using PizzaPlanet.Business.Logic.Models;

namespace PizzaPlanet.Business.Logic.Interfaces
{
    public interface IClientService
    {
        void AddClient(ClientBl client);
        ClientBl CheckYouHaveClient(string clientTelephone);
    }
}