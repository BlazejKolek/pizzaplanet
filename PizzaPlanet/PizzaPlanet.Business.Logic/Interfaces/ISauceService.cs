﻿using PizzaPlanet.Business.Logic.Models;
using System.Collections.Generic;

namespace PizzaPlanet.Business.Logic.Interfaces
{
    public interface ISauceService
    {
        void AddSauce(SauceBl sauce);
        List<SauceBl> ReturnAllSauces();
        SauceBl ReturnPizzaById(int id);
    }
}