﻿using PizzaPlanet.Business.Logic.Models;
using System.Collections.Generic;

namespace PizzaPlanet.Business.Logic.Interfaces
{
    public interface IPizzaService
    {
        void AddIngredientToPizza(PizzaBl newPizza, int idgredient, int quantity);
        void AddPizza(PizzaBl pizza, double priceSizaS, double priceSizaM, double priceSizaL);
        bool AreMoreThanTwoSameIngredientsAdded(PizzaBl pizza, int countingIdgredient);
        void CreatePizza(string name, double priceSizaS, double priceSizaM, double priceSizaL,int sauceToAdd);
        List<PizzaBl> ReturnAllPizzas();
        PizzaBl ReturnPizzaById(int id);
    }
}