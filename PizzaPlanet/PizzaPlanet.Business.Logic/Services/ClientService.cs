﻿using PizzaPlanet.Business.Logic.Interfaces;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal;
using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Services
{
   public class ClientService : IClientService
    {
        private readonly Func<IPizzeriaContext> _pizzeriaContext;

        public ClientService(Func<IPizzeriaContext> pizzeriacontext)
        {
            _pizzeriaContext = pizzeriacontext;
        }

        public void AddClient(ClientBl client)
        {
            if (string.IsNullOrWhiteSpace(client.Name) && string.IsNullOrWhiteSpace(client.Surname) 
                            && string.IsNullOrWhiteSpace(client.Address) & string.IsNullOrWhiteSpace(client.Telephone))
            {
                Console.WriteLine("The data entered is not enough to create a client" );
                return;
            }

            bool clientExist = CheckClientExist(client);
            if (clientExist)
            {
                Console.WriteLine("The customer with personal data and e-mail exists in the database.");
            }
            else
            {
                var clientToAdd = AutoMapper.Mapper.Map<ClientBl, Client>(client);

                using (var context = _pizzeriaContext())
                {
                    context.Clients.Add(clientToAdd);
                    context.PizzeriaSaveChanges();
                }
            }        
        }

        private bool CheckClientExist(ClientBl client)
        {
            using (var context = _pizzeriaContext())
            {
                foreach (var clientToCheck in context.Clients.ToList())
                {
                    if (client.Telephone == clientToCheck.Telephone )
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public ClientBl CheckYouHaveClient(string clientTelephone)
        {
            var newClient = new ClientBl();
            using (PizzeriaContext ctx = new PizzeriaContext())
            {
                foreach (var client in ctx.Clients.ToList())
                {
                    if (clientTelephone == client.Telephone)
                    {
                        return newClient = AutoMapper.Mapper.Map<Client, ClientBl>(client);
                    }
                }
                return null;
            }
        }
    }
}
