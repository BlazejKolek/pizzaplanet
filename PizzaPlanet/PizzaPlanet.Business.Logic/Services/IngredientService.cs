﻿using PizzaPlanet.Business.Logic.Helpers;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using PizzaPlanet.Dal.Models;
using PizzaPlanet.Business.Logic.Interfaces;

namespace PizzaPlanet.Business.Logic.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly Func<IPizzeriaContext> _pizzeriacontext;

        public IngredientService(Func<IPizzeriaContext> pizzeriacontext)
        {
            _pizzeriacontext = pizzeriacontext;
        }

        public void AddIngredient(IngredientBl ingredient)
        {
            if (ingredient.Name == null || string.IsNullOrWhiteSpace(ingredient.Name))
            {
                Console.WriteLine("Cannot add a ingredient without name");
                return;
            }

            var ingredientToAdd = AutoMapper.Mapper.Map<IngredientBl, Ingredient>(ingredient);

            using (var context = _pizzeriacontext())
            {
                foreach (var item in context.Ingredients)
                {
                    if (item.Name.Contains(ingredient.Name))
                    {
                        Console.WriteLine("Ingredient exist in database");
                        return;
                    }
                }
                context.Ingredients.Attach(ingredientToAdd);
                context.Entry(ingredientToAdd).State = EntityState.Unchanged;
                context.Ingredients.Add(ingredientToAdd);
                context.PizzeriaSaveChanges();
            }
        }

        public void GetAllIngredients()
        {
            using (var context = _pizzeriacontext())
            {
                foreach (var ingredient in context.Ingredients.ToList())
                {
                    Console.WriteLine($"- {ingredient.Id} - {ingredient.Name}");
                }
            }
        }

        public IngredientBl ReturnIngredientById(int ingredient)
        {
            using (var context = _pizzeriacontext())
            {
                return AutoMapper.Mapper.Map<Ingredient, IngredientBl>(context.Ingredients.Where(x => x.Id == ingredient).FirstOrDefault());
            }
        }
    }

   
}

