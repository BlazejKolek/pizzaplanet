﻿using PizzaPlanet.Business.Logic.Helpers;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal;
using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PizzaPlanet.Business.Logic.Interfaces;

namespace PizzaPlanet.Business.Logic.Services
{
    public class PizzaService : IPizzaService
    {
        ISauceService _sauceService;
        IIngredientService _ingredientService;
        Func<IPizzeriaContext> _pizzeriaContext;

        public PizzaService(ISauceService sauceService, IIngredientService ingredientService, Func<IPizzeriaContext> pizzeriaContext)
        {
            _sauceService = sauceService;
            _ingredientService = ingredientService;
            _pizzeriaContext = pizzeriaContext;
        }
        public void AddPizza(PizzaBl pizza,double priceSizaS, double priceSizaM, double priceSizaL)
        {
            if (string.IsNullOrWhiteSpace(pizza.Name))
            {
                Console.WriteLine("Wrong input data, you don't have pizza name or your price is not bigger than 0");
                return;
            }

            var addPizza = Mapper.Map<PizzaBl, Pizza>(pizza);

            var listPizza = CreateTreePizzaAndReturnListOfPizzas(addPizza, priceSizaS, priceSizaM, priceSizaL);

            foreach (var pizz in listPizza)
            {
                using (var context = _pizzeriaContext())
                {
                    context.Sauces.Attach(pizz.Sauce);
                    context.Entry(pizz.Sauce).State = EntityState.Unchanged;

                    foreach (var ingredient in pizz.Ingredients)
                    {
                        context.Ingredients.Attach(ingredient.IngredientId);
                        context.Entry(ingredient).State = EntityState.Unchanged;
                        context.PizzaIngredients.Add(ingredient);
                    }

                    context.Pizzas.Add(pizz);
                    context.PizzeriaSaveChanges(); ;
                }
            }          
        }

        private List<Pizza> CreateTreePizzaAndReturnListOfPizzas(Pizza addPizza, double priceSizaS, double priceSizaM, double priceSizaL)
        {
            var listPizza = new List<Pizza>();

            var listPrice = new List<double>() { priceSizaS, priceSizaM, priceSizaL };
            var sizes = new List<Pizza.Size>() { Pizza.Size.S, Pizza.Size.M, Pizza.Size.L };
            var ingredient = addPizza.Ingredients;
            var sauce = addPizza.Sauce;
            var name = addPizza.Name;

            for (int i = 0; i < 3; i++)
            {
                var addNewSizePizza = new Pizza();
                addNewSizePizza.Ingredients = ingredient;
                addNewSizePizza.Name = name;
                addNewSizePizza.Sauce = sauce;
                addNewSizePizza.Price = listPrice[i];
                addNewSizePizza.PizzaSize = sizes[i];
                listPizza.Add(addNewSizePizza);
            }
            return listPizza;
        }

        public List<PizzaBl> ReturnAllPizzas()
        {
            using (var context = _pizzeriaContext())
            {
                return Mapper.Map<List<Pizza>, List<PizzaBl>>(context.Pizzas
                                                            .Include(sauce => sauce.Sauce)
                                                            .Include(ingredients => ingredients.Ingredients.Select(x => x.IngredientId))
                                                            .ToList());
            };
        }
        public PizzaBl ReturnPizzaById(int id)
        {
            using (var context = _pizzeriaContext())
            {
                return Mapper.Map<Pizza, PizzaBl>(context.Pizzas.Where(x => x.Id == id).FirstOrDefault());
            }
        }

        public void AddIngredientToPizza(PizzaBl newPizza, int idgredient,int quantity)
        {
            bool moreThanTwoSameIngredient;
            var ingredientToPizaList = new PizzaIngredientBl();
            var ingredient = new IngredientBl();

            if (moreThanTwoSameIngredient = AreMoreThanTwoSameIngredientsAdded(newPizza, idgredient))
            {
                var ingredientToAdd = _ingredientService.ReturnIngredientById(idgredient);

                if (ingredientToAdd == null)
                {
                    Console.WriteLine($"Idgredient {idgredient} is not available");
                    return;
                }
                else
                {
                    using (PizzeriaContext _context = new PizzeriaContext())
                    {
                        ingredientToPizaList.IngredientId = ingredientToAdd;                       
                        ingredientToPizaList.Quantity = quantity;
                    }

                    newPizza.Ingredients.Add(ingredientToPizaList);
                }
            }
        }

        public void CreatePizza(string name, double priceSizaS, double priceSizaM, double priceSizaL,int sauceToAdd)
        {
            var newPizza = new PizzaBl();
            var ingredientsToPizza = new List<PizzaIngredientBl>();
            var sauceToPizza = new SauceBl();
            int countAllIdgredients = 0;
            string _youWannaEndAddIngredients;

            bool mayHaveCreatePizza = CheckPizzaIsExistAndHaveItemsToCreatePizza(name);
            if (mayHaveCreatePizza == false)
            {
                return;
            }

            do
            {
                _ingredientService.GetAllIngredients();
                
                try
                {
                    Console.WriteLine("Add Idgredient - Enter Id");
                    int idgredient = Int32.Parse(Console.ReadLine());

                    Console.WriteLine("How much ingredients");
                    int howMuchIngredient = Int32.Parse(Console.ReadLine());
                    if (howMuchIngredient > 2)
                    {
                        Console.WriteLine("Too many of the same ingredients");
                        return;
                    }

                    AddIngredientToPizza(newPizza, idgredient, howMuchIngredient);
                }
                catch (Exception)
                {

                    Console.WriteLine("Wrong input data! Ingredient must be a number");
                    countAllIdgredients--;
                }
                
                newPizza.Ingredients.Remove(null);
                countAllIdgredients = newPizza.Ingredients.Select(x => x.IngredientId).Count() * newPizza.Ingredients.Sum(x => x.Quantity);
                if (countAllIdgredients > 7)
                {
                    Console.WriteLine("You have enough ingredients. Ingredients will adding to list.");
                    break;
                }

                Console.WriteLine("You want choose another idgredience enter YES");
                _youWannaEndAddIngredients = Console.ReadLine();
            }
            while (_youWannaEndAddIngredients.ToUpper() == "YES");

            if (newPizza.Ingredients.Count() == 0)
            {
                Console.WriteLine("Cannot create pizza without ingredients");
                return;
            }

            var sauceAdded = _sauceService.ReturnPizzaById(sauceToAdd);

            if (sauceAdded == null)
            {
                Console.WriteLine("Create it not possible beacause we don't have a entry sauce");
                return;
            }
            else
            {
                sauceToPizza = sauceAdded;
            }
            newPizza.Name = name;
            newPizza.Sauce = sauceToPizza;
            AddPizza(newPizza,priceSizaS,priceSizaM,priceSizaL);
        }

        private bool CheckPizzaIsExistAndHaveItemsToCreatePizza(string name)
        {
            using (var context = _pizzeriaContext())
            {
                var howManyIngredient = context.Ingredients.Count();
                var howMuchSauces = _sauceService.ReturnAllSauces().Count();
                if (howManyIngredient == 0 || howMuchSauces == 0)
                {
                    Console.WriteLine("Cannot create pizza without ingredients and sauces.");
                    return false;
                }

                foreach (var pizzas in context.Pizzas)
                {
                    if (pizzas.Name == name )
                    {
                        Console.WriteLine("Pizza exist in database");
                        return false;
                    }
                }
            }
            return true;
        }


        public bool AreMoreThanTwoSameIngredientsAdded(PizzaBl pizza, int countingIdgredient)
        {
            int howMuchIdgredient = 0;

            howMuchIdgredient = pizza.Ingredients.Where(x => x.IngredientId.Id == countingIdgredient).Count() 
                                * pizza.Ingredients.Where(x => x.IngredientId.Id == countingIdgredient).Sum(y => y.Quantity);

            if (howMuchIdgredient >= 2)
            {
                Console.WriteLine("You have more than 2 same idgredients");
                return false;
            }
            return true;
        }
    }
}
