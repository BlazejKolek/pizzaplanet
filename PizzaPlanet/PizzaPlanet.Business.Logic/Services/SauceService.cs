﻿using PizzaPlanet.Business.Logic.Interfaces;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal;
using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PizzaPlanet.Business.Logic.Services
{
    public class SauceService : ISauceService
    {
        private readonly Func<IPizzeriaContext> _pizzeriacontext;

        public SauceService(Func<IPizzeriaContext> pizzeriacontext)
        {
            _pizzeriacontext = pizzeriacontext;
        }

        public void AddSauce(SauceBl sauce)
        {
            if (string.IsNullOrWhiteSpace(sauce.Name) || sauce.Price <= 0)
            {
                Console.WriteLine("Wrong input data, you don't have sauce name or your price is not bigger than 0");
                return;
            }
            var sauceToAdd = AutoMapper.Mapper.Map<SauceBl, Sauce>(sauce);

            using (var context = _pizzeriacontext())
            {
                foreach (var item in context.Sauces)
                {
                    if (item.Name.Contains(sauce.Name))
                    {
                        Console.WriteLine("Sauce exist in database");
                        return;
                    }
                }

                context.Sauces.Add(sauceToAdd);
                context.PizzeriaSaveChanges();
            }
        }

        public List<SauceBl> ReturnAllSauces()
        {
            using (var context = _pizzeriacontext())
            {
                return AutoMapper.Mapper.Map<List<Sauce>, List<SauceBl>>(context.Sauces.ToList());
            }
        }

        public SauceBl ReturnPizzaById(int id)
        {
            using (var context = _pizzeriacontext())
            {
                return AutoMapper.Mapper.Map<Sauce, SauceBl>(context.Sauces.Where(x => x.Id == id).FirstOrDefault());
            }
        }
    }
}
