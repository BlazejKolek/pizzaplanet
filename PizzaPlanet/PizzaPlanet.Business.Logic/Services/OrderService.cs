﻿using PizzaPlanet.Business.Logic.Interfaces;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Dal;
using PizzaPlanet.Dal.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace PizzaPlanet.Business.Logic.Services
{
    public class OrderService : IOrderService
    {
        ISauceService _sauceService;
        Func<IPizzeriaContext> _pizzeriaContext;
        IPizzaService _pizzaService;

        private double pricePizza;

        public IPizzeriaContext PizzeriaContext { get; }

        public OrderService(ISauceService sauceService,Func<IPizzeriaContext> pizzeriacontext, IPizzaService pizzaService)
        {
            _sauceService = sauceService;
            _pizzeriaContext = pizzeriacontext;
            _pizzaService = pizzaService;
        }

        public void AddOrder(OrderBl order)
        {
            bool dataToCreateOrder = CheckInputDataToAddOrderIsNotWrong(order);

            if (dataToCreateOrder)
            {
                var addOrder = AutoMapper.Mapper.Map<OrderBl, Order>(order);
                var modifyClient = AutoMapper.Mapper.Map<ClientBl, Client>(order.Client);

                using (var context = _pizzeriaContext())
                {
                    foreach (var pizza in addOrder.Pizzas)
                    {
                        pizza.Pizza = context.Pizzas.SingleOrDefault(x => x.Id == pizza.Id);
                        context.OrderPizzas.Add(pizza);
                    }

                    foreach (var sauce in addOrder.Sauces)
                    {
                        sauce.Sauce = context.Sauces.SingleOrDefault(x => x.Id == sauce.Sauce.Id);
                        context.OrderSauces.Add(sauce);
                    }

                    addOrder.Client = context.Clients.SingleOrDefault(x => x.Id == addOrder.Client.Id);
                    addOrder.Client.LastOrder = DateTime.Today;

                    context.Orders.Add(addOrder);
                    context.PizzeriaSaveChanges();
                }
            }
            else
            {
                Console.WriteLine("Cannot create order without pizzas and sauces ");
            }
        }

        public bool CheckInputDataToAddOrderIsNotWrong(OrderBl order)
        {
            if (order.Pizzas == null || order.Pizzas.Count() == 0 || order.Sauces == null || order.Sauces.Count() == 0)
            { 
                return false;
            }
            return true;
        }

        public bool CheckHaveAnyPizza()
        {
            int howMuchPizzaWasCreated = _pizzaService.ReturnAllPizzas().Count(); 

            if (howMuchPizzaWasCreated > 0)
            {
                return true;
            }
            return false;
        }

        public double ReturnOrderPrice(OrderBl newOrder)
        {
            double sumPizzaPrice = 0;
            double sumSaucePrice = 0;

            int numberOfPromotion = ReturnNumberOfPromotion(newOrder.OrderDate.DayOfWeek);

            foreach (var pizza in newOrder.Pizzas)
            {
                if (numberOfPromotion == 2)
                {
                    pricePizza = (pizza.Pizza.Price *= pizza.Quantity) / 2;
                }
                else if (numberOfPromotion == 1)
                {
                    if (pizza.Pizza.PizzaSize == PizzaBl.Size.L)
                    {
                        pricePizza = ReturnPricePizzaInSizeM(pizza.Pizza) * pizza.Quantity;
                    }
                    else
                    {
                        pricePizza = pizza.Pizza.Price *= pizza.Quantity;
                    }                      
                }
                else
                {
                    pricePizza = pizza.Pizza.Price *= pizza.Quantity;
                };

                sumPizzaPrice += pricePizza;
            }
           
            foreach (var pizza in newOrder.Sauces.Where(x => x.IsFree == false))
            {
                var priceSauce = pizza.Sauce.Price *= pizza.Quantity;
                sumSaucePrice += priceSauce;
            }

            return Math.Round(sumPizzaPrice + sumSaucePrice, 2);
        }

        public double ReturnPricePizzaInSizeM(PizzaBl pizza)
        {
            return _pizzaService.ReturnAllPizzas().Where(x => x.Name == pizza.Name && x.PizzaSize == PizzaBl.Size.M).Select(x => x.Price).FirstOrDefault();
        }

        public int ReturnNumberOfPromotion(DayOfWeek orderDate)
        {
            switch (orderDate)
            {
                case DayOfWeek.Wednesday:
                    return 1;
                case DayOfWeek.Sunday:
                    return 2;
                case DayOfWeek.Saturday:
                    return 2;
                default:
                    return 0;
            }
        }

        public OrderPizzaBl AddPizzaToOrder(OrderBl newOrder, int pizzaToOrder,int howManyPizzasYouWant)
    {
            var orderPizza = new OrderPizzaBl();

            var pizzaToAdd = _pizzaService.ReturnPizzaById(pizzaToOrder);
            if (pizzaToAdd == null)
            {
                Console.WriteLine("This pizza is not available. Pick pizza from the available list of pizza.");
                return null;
             }
            else
            {
                orderPizza.Pizza = pizzaToAdd;
                orderPizza.Quantity = howManyPizzasYouWant;
                return orderPizza;
            }
        }

        public OrderSauceBl AddSauceToOrder(int sauceToList,int howManySauce,bool isFree)
        {
            var sauce = new OrderSauceBl();
            var sauceToAdd = _sauceService.ReturnPizzaById(sauceToList);
            if (sauceToAdd == null)
            {
                Console.WriteLine("This sauce is not available. Pick sauce from the available list of sauces.");
                return null;
            }
            else
            {
                sauce.Sauce = sauceToAdd;
                sauce.Quantity = howManySauce;
                sauce.IsFree = isFree;
                return sauce;
            }
        }
    }
}

