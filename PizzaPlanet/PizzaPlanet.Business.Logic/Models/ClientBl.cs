﻿using System;

namespace PizzaPlanet.Business.Logic.Models
{
    public class ClientBl
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public DateTime? LastOrder { get; set; }
    }
}