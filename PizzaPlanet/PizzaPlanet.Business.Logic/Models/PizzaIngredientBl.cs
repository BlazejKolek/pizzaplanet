﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Models
{
   public class PizzaIngredientBl
    {
        public int Id { get; set; }
        public IngredientBl IngredientId { get; set; }
        public int Quantity { get; set; }
    }
}
