﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Models
{
    public class OrderPizzaBl
    {
        public int Id { get; set; }
        public PizzaBl Pizza { get; set; }
        public int Quantity { get; set; }
    }
}
