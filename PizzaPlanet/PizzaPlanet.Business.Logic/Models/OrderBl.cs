﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Models
{
   public class OrderBl
    {
        public OrderBl()
        {
            Sauces = new List<OrderSauceBl>();
            Pizzas = new List<OrderPizzaBl>();
        }

        public int OrderId { get; set; }
        public double Price { get; set; }
        public DateTime OrderDate { get; set; }
        public ClientBl Client { get; set; }

        public virtual ICollection<OrderSauceBl> Sauces { get; set; }
        public virtual ICollection<OrderPizzaBl> Pizzas { get; set; }
    }
}
