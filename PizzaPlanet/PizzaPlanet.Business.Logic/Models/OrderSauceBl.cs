﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Models
{
    public class OrderSauceBl
    {
        public int Id { get; set; }
        public SauceBl Sauce { get; set; }
        public int Quantity { get; set; }
        public bool IsFree { get; set; }
    }
}
