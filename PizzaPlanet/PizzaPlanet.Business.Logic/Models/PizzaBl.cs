﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Models
{
   public class PizzaBl
    {
        public PizzaBl()
        {
            Ingredients = new List<PizzaIngredientBl>();
        }
        public enum Size
        {
            S,
            M,
            L
        };
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Size PizzaSize { get; set; }
        public ICollection<PizzaIngredientBl> Ingredients { get; set; }
        public SauceBl Sauce { get; set; }

    }
}
