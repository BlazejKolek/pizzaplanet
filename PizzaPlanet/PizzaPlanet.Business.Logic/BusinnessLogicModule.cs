﻿using Ninject;
using Ninject.Modules;
using PizzaPlanet.Business.Logic.Interfaces;
using PizzaPlanet.Business.Logic.Services;
using PizzaPlanet.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic
{
   public class BusinnessLogicModule : NinjectModule
    {
        public override void Load()
        {
           Kernel.Bind<IClientService>().To<ClientService>();
           Kernel.Bind<IIngredientService>().To<IngredientService>();
           Kernel.Bind<IOrderService>().To<OrderService>();
           Kernel.Bind<IPizzaService>().To<PizzaService>();
           Kernel.Bind<ISauceService>().To<SauceService>();
           Kernel.Load(new DalModule());
        }
    }
}
