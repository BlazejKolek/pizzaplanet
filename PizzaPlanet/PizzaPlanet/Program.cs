﻿using System;
using System.Collections.Generic;
using System.Threading;
using PizzaPlanet.Cli.Helpers;
using PizzaPlanet.Business.Logic.Services;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Cli.Operations;
using System.Linq;
using PizzaPlanet.Business.Logic.Helpers;
using Ninject;
using PizzaPlanet.Business.Logic;
using PizzaPlanet.Business.Logic.Interfaces;

namespace PizzaPlanet.Cli
{
    internal class Program
    {
        private IMenu _menu;
        private IoHelper _ioHelper = new IoHelper();

        private ISauceService _sauceService;
        private IIngredientService _ingredientService;
        private IPizzaService _pizzaService;
        private IOrderService _orderService;
        private IClientService _clientService;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome in PizzaPlanet!");

            IKernel kernel = new StandardKernel(new CliModule());
            var run = kernel.Get<Program>();
            run.SetCommandMenu();

            do
            {
               run.ProgramLoop();
            } while (true);
        }

        private void ProgramLoop()
        {
            _menu.PrintAllCommands();
            _menu.Execute(Console.ReadLine());
        }

        public Program(ISauceService sauceService, IIngredientService ingredientService, IPizzaService pizzaService, IOrderService orderService, IClientService clientService,IMenu menu)
        {
            AutoMapperModel.CreateMap();

            _sauceService = sauceService;
            _pizzaService = pizzaService;
            _orderService = orderService;
            _clientService = clientService;
            _menu = menu;
            _ingredientService = ingredientService;
        }

        private void SetCommandMenu()
        {
            _menu.SetCommand(AddIngredient, "Add ingredient");
            _menu.SetCommand(AddSauce, "Add sauce");
            _menu.SetCommand(AddPizza, "Add pizza");
            _menu.SetCommand(ShowMenu, "Show menu");
            _menu.SetCommand(AddClient, "Add Client");
            _menu.SetCommand(GenerateOrder, "Order");
            _menu.SetCommand(Quit, "Quit");
        }

        private void AddClient()
        {
            var name = StringOperation.ReturnTextFormUser("Enter name of client");
            var surname = StringOperation.ReturnTextFormUser("Enter surname of client");
            var email = StringOperation.ReturnTextFormUser("Enter email");
            var telephone = StringOperation.ReturnTextFormUser("Enter telephone number");
            var address = StringOperation.ReturnTextFormUser("Enter address");

            var newClient = new ClientBl()
            {
                Name = name,
                Surname = surname,
                Email = email,
                Telephone = telephone,
                Address = address,
                LastOrder = null
            };
            _clientService.AddClient(newClient);
        }

        private void AddIngredient()
        {
            var name = StringOperation.ReturnTextFormUser("Enter name of ingredient");
            var newIngredient = new IngredientBl
            {
                Name = name
            };
            _ingredientService.AddIngredient(newIngredient);
        }

        private void AddSauce()
        {
            var name = StringOperation.ReturnTextFormUser("Enter the name of sauce :");
            try
            {
                var price = Convert.ToDouble(StringOperation.ReturnTextFormUser("Enter the price of the sauce ( Acept value is x.xx where x is number) :"));
                var newSauce = new SauceBl()
                {
                    Name = name,
                    Price = price
                };
               
                _sauceService.AddSauce(newSauce);
            }
            catch (Exception)
            {
                Console.WriteLine("Badly entered data format.");
            } 
        }

        private void AddPizza()
        {
            var name = StringOperation.ReturnTextFormUser("Enter the name of the pizza :");
            try
            {
                double priceSizaS = Convert.ToDouble(StringOperation.ReturnTextFormUser("Enter the price of small size"));
                double priceSizaM = Convert.ToDouble(StringOperation.ReturnTextFormUser("Enter the price of medium size"));
                double priceSizaL = Convert.ToDouble(StringOperation.ReturnTextFormUser("Enter the price of large size"));

                var sauce = _sauceService.ReturnAllSauces();
                _ioHelper.PrintAllSauces(sauce);

                int sauceToAdd = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter the Id sauce under the cheese"));

                _pizzaService.CreatePizza(name, priceSizaS, priceSizaM, priceSizaL, sauceToAdd);
            }
            catch (Exception e) 
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("We cannot create a pizza because intput data is wrong");
            }
        }

        private void ShowMenu()
        {
           var listPizza = _pizzaService.ReturnAllPizzas();
            _ioHelper.PrintAllPizzas(listPizza);
        }

        private void GenerateOrder()
        {
            var newOrder = new OrderBl();
            var pizzasToOrderList = new List<OrderPizzaBl>();
            var sauceToOrderList = new List<OrderSauceBl>();
            bool haveAnyPizza = _orderService.CheckHaveAnyPizza();

            if (haveAnyPizza == false)
            {
                Console.WriteLine("You haven't any pizzas to create order");
                return;
            }

            GetInformationAboutClientToComleteOrder(newOrder);
            ShowMenu();

            newOrder.OrderDate = DateTime.Today;
            
            GetDataFromUserAndAddPizzaToOrder(newOrder, pizzasToOrderList);

            int howManyFreeSaucesToOrder = (newOrder.Pizzas.Where(x => x.Pizza.PizzaSize == PizzaBl.Size.L).Sum(y =>y.Quantity) * 2)
                                            + (newOrder.Pizzas.Where(x => x.Pizza.PizzaSize == PizzaBl.Size.S).Sum(y => y.Quantity))
                                            + (newOrder.Pizzas.Where(x => x.Pizza.PizzaSize == PizzaBl.Size.M).Sum(y => y.Quantity));

            FreeSaucesToOrder(newOrder, sauceToOrderList, howManyFreeSaucesToOrder);

            bool youWannaAddPaidSauce = _ioHelper.ReturnBoolFromString("You wanna add paid sauce ?");
            if (youWannaAddPaidSauce)
            {
                PaidSauceToOrder(newOrder, sauceToOrderList);
            }

            newOrder.Price = _orderService.ReturnOrderPrice(newOrder);
            newOrder.OrderDate = DateTime.Today;
            _orderService.AddOrder(newOrder);
        }

        private void GetInformationAboutClientToComleteOrder(OrderBl newOrder)
        {
            ClientBl clitnToOrder = null;
            do
            {
                var clientTelephone = StringOperation.ReturnTextFormUser("Please enter your telephone number");
                clitnToOrder  = _clientService.CheckYouHaveClient(clientTelephone);
                if (clitnToOrder == null)
                {
                    clitnToOrder = CustomerForm(clientTelephone);
                }
                newOrder.Client = clitnToOrder;
            } while (newOrder.Client == null);
        }

        private ClientBl CustomerForm(string clientTelephone)
        {
            Console.WriteLine("Welcome to the client creation form");
            var name = StringOperation.ReturnTextFormUser("Enter name of client");
            var surname = StringOperation.ReturnTextFormUser("Enter surname of client");
            var email = StringOperation.ReturnTextFormUser("Enter email");
            var address = StringOperation.ReturnTextFormUser("Enter address");

            var newClient = new ClientBl()
            {
                Name = name,
                Surname = surname,
                Email = email,
                Telephone = clientTelephone,
                Address = address,
                LastOrder = DateTime.Today
            };
            _clientService.AddClient(newClient);
            return newClient;
        }

        public void FreeSaucesToOrder(OrderBl newOrder,List<OrderSauceBl> sauceToOrderList,int howManyFreeSaucesToOrder)
        {
            int howMuchSaucesAdded = 0;
            var allSauces = _sauceService.ReturnAllSauces();
            _ioHelper.PrintAllSauces(allSauces);
            do
            {
                try
                {
                    int sauceToList = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter number of sauce"));
                    int howManySauce = Convert.ToInt32(StringOperation.ReturnTextFormUser("How many ?"));
                    if (howMuchSaucesAdded + howManySauce > howManyFreeSaucesToOrder)
                    {
                        Console.WriteLine($"You have add to much free sauces, can add {howManyFreeSaucesToOrder - howMuchSaucesAdded} more free sauces");
                    }
                    else
                    {
                        sauceToOrderList.Add(_orderService.AddSauceToOrder(sauceToList, howManySauce,true));
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("There is no sauce with the given number");
                }
                
                howMuchSaucesAdded = sauceToOrderList.Sum(x => x.Quantity);
            } while (!(howMuchSaucesAdded == howManyFreeSaucesToOrder));
            newOrder.Sauces = sauceToOrderList;
        }

        private void PaidSauceToOrder(OrderBl newOrder, List<OrderSauceBl> sauceToOrderList)
        {
            bool youWannaAddNextPaidSauce;
            _sauceService.ReturnAllSauces();
            do
            {
                try
                {
                    int sauceToList = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter number of sauce"));
                    int howManySauce = Convert.ToInt32(StringOperation.ReturnTextFormUser("How many ?"));
                    sauceToOrderList.Add(_orderService.AddSauceToOrder(sauceToList, howManySauce,false));
                }
                catch (Exception)
                {
                    Console.WriteLine("There is no sauce with the given number");
                }
                youWannaAddNextPaidSauce = _ioHelper.ReturnBoolFromString("Do you wanna add another paid sauce?");
            } while (youWannaAddNextPaidSauce);
            newOrder.Sauces = sauceToOrderList;
        }

        private void GetDataFromUserAndAddPizzaToOrder(OrderBl newOrder,List<OrderPizzaBl> pizzasToOrderList)
        {
            bool checkYouHaveAddAnotherPizza;
            var datOrder = newOrder.OrderDate.DayOfWeek;
           
            do
            {
                try
                {
                    int whatPizzaYouWantAdd = Convert.ToInt32(StringOperation.ReturnTextFormUser("Provide a pizza number"));
                    int howManyPizzasYouWant = Convert.ToInt32(StringOperation.ReturnTextFormUser($"How many pizzas with Id {whatPizzaYouWantAdd} ?"));

                    pizzasToOrderList.Add(_orderService.AddPizzaToOrder(newOrder, whatPizzaYouWantAdd, howManyPizzasYouWant));
                }
                catch (Exception)
                {
                    Console.WriteLine("There is no pizza with the given number");
                    return;
                }
                
                checkYouHaveAddAnotherPizza = _ioHelper.ReturnBoolFromString("If you wanna add next pizza enter Yes");
            } while (checkYouHaveAddAnotherPizza);

            newOrder.Pizzas = pizzasToOrderList;
        }

        private void Quit()
        {
            Console.WriteLine("Good Bye!");
            Thread.Sleep(500);
            Environment.Exit(0);
        }
    }
}

