﻿using Ninject;
using Ninject.Modules;
using PizzaPlanet.Business.Logic;
using PizzaPlanet.Cli.Helpers;

namespace PizzaPlanet.Cli
{
    internal class CliModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Load(new BusinnessLogicModule());
        }
    }
}