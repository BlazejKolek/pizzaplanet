﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Cli.Helpers
{
   internal class Menu : IMenu
    {
        private Dictionary<string, Action> _commands = new Dictionary<string, Action>();

        public void SetCommand(Action command, string commandId)
        {
            if (_commands.ContainsKey(commandId))
            {
                throw new Exception($"Command with Id {commandId} is not exist");
            }
            _commands.Add(commandId, command);
        }

        public void Execute(string commandId)
        {
            if (!_commands.ContainsKey(commandId))
            {
                Console.WriteLine($"There is no command with id {commandId}!");
                return;
            }
            var chosenCommand = _commands[commandId];
            chosenCommand();
        }

        public void PrintAllCommands()
        {
            Console.WriteLine("Available option:");
            foreach (var item in _commands)
            {
                Console.WriteLine($"- {item.Key}");
            }
        }
    }
}
