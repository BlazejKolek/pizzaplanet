﻿using PizzaPlanet.Business.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Cli.Helpers
{
    internal class IoHelper
    {
        public bool ReturnBoolFromString(string message)
        {
            Console.WriteLine(message);
            string value = Console.ReadLine();
            if (value.ToUpper() !="YES") { return false;} else {return true;}
        }

        public void PrintAllPizzas(IEnumerable<PizzaBl> pizzas)
        {
            foreach (var pizza in pizzas)
            {
                Console.WriteLine($"-Id :{pizza.Id} - {pizza.Name} in {pizza.PizzaSize} size " +
                                    $"with {pizza.Sauce.Name} under the cheese , for {pizza.Price} " +
                                    $"and have ingredients {string.Join(", ", pizza.Ingredients.Select(x => String.Format("{1} times {0}", x.IngredientId.Name, x.Quantity)))}");
            }
        }

        public void PrintAllSauces(IEnumerable<SauceBl> sauces)
        {
            foreach (var sauce in sauces)
            {
                Console.WriteLine($"- {sauce.Id} - {sauce.Name}");
            }
        }
    }
}
