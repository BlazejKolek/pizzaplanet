﻿using System;

namespace PizzaPlanet.Cli.Helpers
{
    internal interface IMenu
    {
        void Execute(string commandId);
        void PrintAllCommands();
        void SetCommand(Action command, string commandId);
    }
}