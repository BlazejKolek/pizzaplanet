﻿using Moq;
using NUnit.Framework;
using PizzaPlanet.Business.Logic.Interfaces;
using PizzaPlanet.Business.Logic.Models;
using PizzaPlanet.Business.Logic.Services;
using PizzaPlanet.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Logic.Tests
{
    [TestFixture]
    class OrderServiceTests
    {
        public OrderBl OrderToTest(DateTime orderDate)
        {
            var newOrder = new OrderBl()
            {
                OrderDate = orderDate,
                Pizzas = new List<OrderPizzaBl>()
                {
                   new OrderPizzaBl()
                   { Id = 1,
                          Pizza = new PizzaBl()
                          {
                                Id = 1,
                                Name = "Diablo",
                                PizzaSize = PizzaBl.Size.L,
                                Price = 23 ,
                                Sauce = new SauceBl()
                                {
                                    Id = 1,
                                    Name = "Garlic",
                                    Price = 3
                                },
                                Ingredients = new List<PizzaIngredientBl>()
                                {
                                    new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                                    new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                                }
                          },
                          Quantity = 1
                   },
                   new OrderPizzaBl()
                   {
                        Id = 1,
                        Pizza = new PizzaBl()
                        {
                            Id = 2,
                            Name = "Diablo",
                            PizzaSize = PizzaBl.Size.M,
                            Price = 16 ,
                            Sauce = new SauceBl()
                            {
                                Id = 1,
                                Name = "Garlic",
                                Price = 3
                            },
                            Ingredients = new List<PizzaIngredientBl>()
                            {
                                new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                                new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                            }
                        },
                        Quantity = 1
                   }
                },
                Sauces = new List<OrderSauceBl>()
               {
                  new OrderSauceBl(){Id = 1,IsFree = true,Sauce = new SauceBl(){Id = 1,Name = "Garlic",Price = 3},Quantity = 3},
                  new OrderSauceBl(){Id = 1,IsFree = false,Sauce = new SauceBl(){Id = 1,Name = "Garlic",Price = 3},Quantity = 2},
               }
            };
            return newOrder;
        }
        public List<PizzaBl> ListPizzasToTest()
        {
            var listPizzas = new List<PizzaBl>()
            {
                new PizzaBl()
                {
                    Id = 1,
                    Name = "Diablo",
                    PizzaSize = PizzaBl.Size.L,
                    Price = 23 ,
                    Sauce = new SauceBl()
                    {
                        Id = 1,
                        Name = "Garlic",
                        Price = 3
                    },
                    Ingredients = new List<PizzaIngredientBl>()
                    {
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                    }
                },
                new PizzaBl()
                {
                    Id = 2,
                    Name = "Diablo",
                    PizzaSize = PizzaBl.Size.M,
                    Price = 18 ,
                    Sauce = new SauceBl()
                    {
                        Id = 1,
                        Name = "Garlic",
                        Price = 3
                    },
                    Ingredients = new List<PizzaIngredientBl>()
                    {
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                    }
                }
            };
            return listPizzas;
        }

        public OrderPizzaBl PureOrderPizzaToTest()
        {
            var orderPizzaTest = new OrderPizzaBl()
            {
                Pizza = new PizzaBl()
                {
                    Id = 1,
                    Name = "Diablo",
                    PizzaSize = PizzaBl.Size.L,
                    Price = 23,
                    Sauce = new SauceBl()
                    {
                        Id = 1,
                        Name = "Garlic",
                        Price = 3
                    },
                    Ingredients = new List<PizzaIngredientBl>()
                    {
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                    }
                },
                Quantity = 3
            };
            return orderPizzaTest;
        }

        public PizzaBl PizzaTest()
        {
            var pizzaTest = new PizzaBl()
            {
                Id = 1,
                Name = "Diablo",
                PizzaSize = PizzaBl.Size.L,
                Price = 23,
                Sauce = new SauceBl()
                {
                    Id = 1,
                    Name = "Garlic",
                    Price = 3
                },
                Ingredients = new List<PizzaIngredientBl>()
                    {
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                    }
            };
            return pizzaTest;
        }
        [Test]
        public void CheckHaveAnyPizza_ValidInput_ValidOutput()
        {
            //Assert
            var pizzaService = new Mock<IPizzaService>();
            pizzaService.Setup(x => x.ReturnAllPizzas()).Returns(ListPizzasToTest());
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act 
            var result = orderService.CheckHaveAnyPizza();

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void ReturnCorrectPricePizzaInSizeM()
        {
            //Arange
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            var allPizzas = pizzaService.Setup(x => x.ReturnAllPizzas()).Returns(ListPizzasToTest());

            var pizzaInLSize = new PizzaBl()
            {
                Id = 1,
                Name = "Diablo",
                PizzaSize = PizzaBl.Size.L,
                Price = 23,
                Sauce = new SauceBl()
                {
                    Id = 1,
                    Name = "Garlic",
                    Price = 3
                },
                Ingredients = new List<PizzaIngredientBl>()
                    {
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 1,Name ="Onion" },Quantity = 2},
                        new PizzaIngredientBl(){IngredientId = new IngredientBl(){Id = 2,Name ="Salami" },Quantity = 1}
                    }
            };

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act
            var result = orderService.ReturnPricePizzaInSizeM(pizzaInLSize);

            //Assert
            Assert.AreEqual(18, result);
        }

        [Test]
        public void ReturnWrongNumberOfWeek()
        {
            //Arrange
            var orderDayOfWeek = DayOfWeek.Monday;
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act
            var result = orderService.ReturnNumberOfPromotion(orderDayOfWeek);

            //Assert
            Assert.AreNotEqual(2, result);
        }

        [Test]
        public void CheckIfAddEmptyListPizzaReturnFalse()
        {
            //Arrange
            var order = new OrderBl();
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act
            var result = orderService.CheckInputDataToAddOrderIsNotWrong(order);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckOrderPriceCountGood()
        {
            //Arrange
            var orderDateMonday = new DateTime(2019, 03, 11); //It's a monday to check how to will behave price when we don't have promotion.
            var orderMonday = OrderToTest(orderDateMonday);
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act
            var resultMonday = orderService.ReturnOrderPrice(orderMonday);

            //Assert
            Assert.AreEqual(45, resultMonday);
        }

        [Test]
        public void CheckOrderPriceOnWednesdayReturnGoodPrice_ValidInput_ValidOutput()
        {
            //Arrange
            var orderDateWednesday = new DateTime(2019, 03, 6); //It's a wednesday to check how to will behave price when we have Wednesday promotion.  
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            pizzaService.Setup(x => x.ReturnAllPizzas()).Returns(ListPizzasToTest());

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);
            var orderWednesday = OrderToTest(orderDateWednesday);

            //Act
            var resultWednesday = orderService.ReturnOrderPrice(orderWednesday);

            //Assert
            Assert.AreEqual(40, resultWednesday);
        }

        [Test]
        public void CheckOrderPriceOnWeekendReturnGoodPrice_ValidInput_ValidOutput()
        {
            //Arrange
            var orderDateWeekend = new DateTime(2019, 03, 10); //It's a Sunday to check how to will behave price when we have Wednesday promotion.
            var orderWeekend = OrderToTest(orderDateWeekend);
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();

            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            //Act
            var resultWeeked = orderService.ReturnOrderPrice(orderWeekend);

            //Assert
            Assert.AreEqual(25.5, resultWeeked);
        }

        [Test]
        public void ReturnGoodInformationAboutOrderPizza_ValidInput_ValidOutput()
        {
            //Arrange
            var newOrder = new OrderBl();

            var orderPizzaTest = PureOrderPizzaToTest();

            var pizzaTest = PizzaTest();
            
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            pizzaService.Setup(x => x.ReturnAllPizzas()).Returns(ListPizzasToTest);

            pizzaService.Setup(y => y.ReturnPizzaById(1)).Returns(pizzaTest);

            //Act
            var result = orderService.AddPizzaToOrder(newOrder, 1, 3);

            //Assert
            Assert.AreEqual(orderPizzaTest.Id, result.Id);

            Assert.AreEqual(orderPizzaTest.Pizza.Ingredients.Select(x => x.IngredientId.Id), result.Pizza.Ingredients.Select(x => x.IngredientId.Id));
            Assert.AreEqual(orderPizzaTest.Pizza.Ingredients.Select(x => x.IngredientId.Name), result.Pizza.Ingredients.Select(x => x.IngredientId.Name));
            Assert.AreEqual(orderPizzaTest.Pizza.Ingredients.Select(x => x.Quantity), result.Pizza.Ingredients.Select(x => x.Quantity));

            Assert.AreEqual(orderPizzaTest.Pizza.Name, result.Pizza.Name);
            Assert.AreEqual(orderPizzaTest.Pizza.PizzaSize, result.Pizza.PizzaSize);
            Assert.AreEqual(orderPizzaTest.Pizza.Price, result.Pizza.Price);

            Assert.AreEqual(orderPizzaTest.Pizza.Sauce.Id, result.Pizza.Sauce.Id);
            Assert.AreEqual(orderPizzaTest.Pizza.Sauce.Name, result.Pizza.Sauce.Name);
            Assert.AreEqual(orderPizzaTest.Pizza.Sauce.Price, result.Pizza.Sauce.Price);

            Assert.AreEqual(orderPizzaTest.Quantity, result.Quantity);
        }

        [Test]
        public void ReturnGoodInformationAboutOrderSauce_ValidInput_ValidOutput()
        {
            //Arrange
            var newOrder = new OrderBl();
            var orderSauceTest = new OrderSauceBl()
            {
                Sauce = new SauceBl() { Id = 1, Name = "Garlic", Price = 3 },
                Quantity = 2
            };

            var listSauces = new List<SauceBl>() { new SauceBl() { Id = 1, Name = "Garlic", Price = 3 }, new SauceBl() { Id = 2, Name = "Hot", Price = 4 } };
            var sauceToTest = new SauceBl() { Id = 1, Name = "Garlic", Price = 3 };
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            sauceService.Setup(x => x.ReturnAllSauces()).Returns(listSauces);
            sauceService.Setup(x => x.ReturnPizzaById(1)).Returns(sauceToTest);

            //Act
            var result = orderService.AddSauceToOrder(1, 2, false);

            //Assert
            Assert.AreEqual(orderSauceTest.Id, result.Id);
            Assert.AreEqual(orderSauceTest.IsFree, result.IsFree);
            Assert.AreEqual(orderSauceTest.Quantity, result.Quantity);

            Assert.AreEqual(orderSauceTest.Sauce.Id, result.Sauce.Id);
            Assert.AreEqual(orderSauceTest.Sauce.Name, result.Sauce.Name);
            Assert.AreEqual(orderSauceTest.Sauce.Price, result.Sauce.Price);
        }

        [Test]
        public void TestingIfWeEnterPizzaNumberWhoDoesntExistReciveNullSaucesAndZeroValueLeadToDontAddingApizza()
        {
            //Arrange
            var newOrder = new OrderBl();
            var pizzaTest = new PizzaBl();

            var orderPizzaTest = PureOrderPizzaToTest();
            
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            pizzaService.Setup(x => x.ReturnAllPizzas()).Returns(ListPizzasToTest);

            pizzaService.Setup(y => y.ReturnPizzaById(1)).Returns(pizzaTest);

            //Act
            var result = orderService.AddPizzaToOrder(newOrder, 1, 3);

            //Assert
            //Assert.AreEqual(orderPizzaTest.Id, result.Id);
            Assert.AreEqual(0,result.Id);
            Assert.AreEqual(3,result.Quantity);
            Assert.AreEqual(0,result.Pizza.Id);
            Assert.IsNull(result.Pizza.Sauce);
            Assert.AreEqual(0,result.Pizza.Price);

        }

        [Test]
        public void TestingIfWeEnterSauceNumberWhoDoesntExistReciveNullAndZeroValueLeadToDontAddingApizza()
        {
            //Arrange
            var newOrder = new OrderBl();
            var orderSauceTest = new OrderSauceBl()
            {
                Sauce = new SauceBl() { Id = 1, Name = "Garlic", Price = 3 },
                Quantity = 2
            };

            var listSauces = new List<SauceBl>() { new SauceBl() { Id = 1, Name = "Garlic", Price = 3 }, new SauceBl() { Id = 2, Name = "Hot", Price = 4 } };
            var sauceToTest = new SauceBl();
            var sauceService = new Mock<ISauceService>();
            var context = new Mock<IPizzeriaContext>();
            var pizzaService = new Mock<IPizzaService>();
            var orderService = new OrderService(sauceService.Object, () => context.Object, pizzaService.Object);

            sauceService.Setup(x => x.ReturnAllSauces()).Returns(listSauces);
            sauceService.Setup(x => x.ReturnPizzaById(1)).Returns(sauceToTest);

            //Act
            var result = orderService.AddSauceToOrder(1, 2, false);

            //Assert
            Assert.AreEqual(0, result.Id);
            Assert.AreEqual(2, result.Quantity);
            Assert.AreEqual(0,result.Sauce.Id);
            Assert.AreEqual(0, result.Sauce.Price);
            Assert.IsNull(result.Sauce.Name);
        }
    }
}