﻿using Ninject.Modules;
using PizzaPlanet.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.DAL
{
   public class DalModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IPizzeriaContext>().ToMethod(x => new PizzeriaContext());
        }
    }
}
