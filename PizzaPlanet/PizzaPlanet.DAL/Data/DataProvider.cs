﻿using PizzaPlanet.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Data
{
   static class DataProvider
    {
        public static List<Ingredient> IgredientsList { get; set; } = new List<Ingredient>();
        public static List<Sauce> SaucesList { get; set; } = new List<Sauce>();
        public static List<Pizza> PizzasList { get; set; } = new List<Pizza>();
        public static List<Order> OrderList { get; set; } = new List<Order>();

        public static void ReturnIdgredientsList()
        {
            Console.WriteLine();
            Console.WriteLine("It's a list available igredients");
            Console.WriteLine();

            foreach (var item in IgredientsList)
            {
                Console.WriteLine(item.Name);
            }
        }

        public static void ReturnISaucesList()
        {
            Console.WriteLine();
            Console.WriteLine("It's a list available sauces");
            Console.WriteLine();

            foreach (var item in SaucesList)
            {
                Console.WriteLine($"{item.Name} for {item.Price.ToString("0.00")}");
            }
        }

        public static void ShowAllPizas()
        {
            Console.WriteLine();
            Console.WriteLine("It's a list available pizza");
            Console.WriteLine();
            foreach (var item in PizzasList)
            {
                Console.WriteLine($"Pizza {item.Name} for {item.Price.ToString("0.00")} $ and have idgredience {string.Join(", ", item.Ingredients.Select(x => x.Name.ToUpper().ToString()))} and the {item.Sauce.Name} sauce under the cheese.");
            }
        }

       
    }
}
            




