﻿using PizzaPlanet.Dal.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PizzaPlanet.Dal.Models
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}