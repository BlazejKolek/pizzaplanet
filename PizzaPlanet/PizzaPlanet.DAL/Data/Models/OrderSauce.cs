﻿using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Dal.Data.Models
{
    public class OrderSauce
    {
        public int Id { get; set; }
        public Sauce Sauce { get; set; }
        public int Quantity { get; set; }
        public bool IsFree { get; set; }
    }
}
