﻿using PizzaPlanet.Dal.Data.Models;
using System;
using System.Collections.Generic;

namespace PizzaPlanet.Dal.Models
{
    public class Order
    {
        public Order()
        {
            Sauces = new List<OrderSauce>();
            Pizzas = new List<OrderPizza>();
        }

        public int OrderId { get; set; }
        public double Price { get; set; }
        public DateTime OrderDate { get; set; }
        public Client Client { get; set; }

        public virtual ICollection<OrderSauce> Sauces { get; set; }
        public virtual ICollection<OrderPizza> Pizzas { get; set; }

    }
}

