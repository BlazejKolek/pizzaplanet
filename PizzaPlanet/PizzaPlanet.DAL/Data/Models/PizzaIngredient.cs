﻿using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Dal.Data.Models
{
   public class PizzaIngredient
    {
        public int Id { get; set; }
        public Ingredient IngredientId { get; set; }
        public int Quantity { get; set; }
    }
}
