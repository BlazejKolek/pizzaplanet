﻿using PizzaPlanet.Dal.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PizzaPlanet.Dal.Models
{
    public class Pizza
    {
        public Pizza()
        {
            Ingredients = new List<PizzaIngredient>();
        }
        public enum Size
        {
            S,
            M,
            L
        };

        public int Id { get; set; }
        public string Name { get; set; }
        public Size PizzaSize { get; set; }
        public double Price { get; set; }
        public ICollection<PizzaIngredient> Ingredients { get; set; }
        public Sauce Sauce { get; set; }

    }
}

