﻿using PizzaPlanet.Dal.Data.Models;
using PizzaPlanet.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace PizzaPlanet.Dal
{
    public class PizzeriaContext : DbContext, IPizzeriaContext
    {
        public PizzeriaContext() :base("PizzeriaDBConnectionString")
        {

        }

        public void PizzeriaSaveChanges()
        {
            this.SaveChanges();
        }

        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Sauce> Sauces { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<PizzaIngredient> PizzaIngredients { get; set; }
        public DbSet<OrderSauce> OrderSauces { get; set; }
        public DbSet<OrderPizza> OrderPizzas { get; set; }
        public Func<PizzeriaContext> Context { get; }
    }
}
