﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using PizzaPlanet.Dal.Data.Models;
using PizzaPlanet.Dal.Models;

namespace PizzaPlanet.Dal
{
    public interface IPizzeriaContext : IDisposable
    {
        DbSet<Client> Clients { get; set; }
        DbSet<Ingredient> Ingredients { get; set; }
        DbSet<OrderPizza> OrderPizzas { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<OrderSauce> OrderSauces { get; set; }
        DbSet<PizzaIngredient> PizzaIngredients { get; set; }
        DbSet<Pizza> Pizzas { get; set; }
        DbSet<Sauce> Sauces { get; set; }

        void PizzeriaSaveChanges();
        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}